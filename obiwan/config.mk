FIRMWARE_IMAGES := \
    abl \
    aop \
    asusfw \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    modem \
    multiimgoem \
    qupfw \
    tz \
    uefisecapp \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
